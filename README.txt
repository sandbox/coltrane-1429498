Example JSON feed from association.drupal.org

{
  "orders" : [
    {
      "order" : {
        "order id" : "3",
        "title" : "DrupalCon Denver 2012 registration",
        "name" : "coltrane",
        "nid" : "1484",
        "uid" : "3897",
        "full name" : "ben jeavons"
      }
    },
    {
      "order" : {
        "order id" : "2",
        "title" : "DrupalCon Denver 2012 registration",
        "name" : "admin-ado",
        "nid" : "1484",
        "uid" : "1"
      }
    }
  ]
}